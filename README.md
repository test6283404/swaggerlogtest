程式的使用說明書
===

程式概述
---
這是用來查詢特定User的程式，您將輸入特定的**UserID(1~12)**，得到的結果會是該User的**名與姓**，以及該User的**email**。同時，輸入其他內容
(非1~12)，將會**終止程式**，這點還請您多多留意。

程式結構說明
---
以 MVC 架構完成，分別為 Controller Service Model. 

Controller 部分是:

    1. 我在 dependency 有添加 springdoc-openapi-starter-webmvc-ui

Service 部分是:

    1. 您將注意到我使用 while迴圈 來做為程式主要的邏輯。
    2. 同時，我使用 try catch 的方式，設定特定的條件(輸入非1~12)，來終止 while迴圈。
    3. Scanner 接收使用者輸入的字，再以與預設好的url用拼接 String 的方式，得到特定的網址
    4. URI 使用 create(特定的網址)，藉由 HttpRequest newBuilder(URI).build() 來得到特定的 HttpRequest
    5. 透過 HttpClient newHttpClient().send(請求, 回復).body() 取得 String 字串
    6. 用 gson JsonParser parse(字串).getAsJsonObject() 將回傳的 String 轉成 JsonObject
    7. 用 gson JsonObject jsonObject.getAsJsonObject("你要的 json key") 取得特定 json
    8. 最後，用 Gson().fromJson(特定 json, 特定物件.class) 取得 特定物件
    9. 使用特定物件 getter and setter
Users 部分是:

    1. 屬於特定物件，有關於您所接收的 json value
    2. 使用 gson @SerializedName 來解決 java 命名規則


Maven 配置
---

* PowerShell執行 會建出maven專案

  `mvn archetype:generate "-DgroupId=com.mycompany.app" "-DartifactId=my-app" "-DarchetypeArtifactId=maven-archetype-quickstart" "-DarchetypeVersion=1.4" "-DinteractiveMode=false"`
* Java 檔放入 src/main/java/com/mycompany/app 底下
* 添加 dependency

  `<dependency>
  <groupId>com.google.code.gson</groupId>
  <artifactId>gson</artifactId>
  <version>2.10.1</version>
  </dependency>
  `
* 添加 plugin

  `<plugin>
  <artifactId>maven-assembly-plugin</artifactId>
  <version>3.6.0</version>
  <configuration>
  <archive>
  <manifest>
  <mainClass>com.mycompany.app.Action</mainClass>
  </manifest>
  </archive>
  <descriptorRefs>
  <descriptorRef>jar-with-dependencies</descriptorRef>
  </descriptorRefs>
  </configuration>
  <executions>
  <execution>
  <id>make-assembly</id>
  <phase>package</phase>
  <goals>
  <goal>single</goal>
  </goals>
  </execution>
  </executions>
  </plugin>`


操作說明
---
### 從 git 下載
1. git bush 執行在您想要的位置
   `$ git clone https://gitlab.com/test6283404/test001.git`
2. PowerShell 執行
    - `cd 到您 clone 的位置`
    - `mvn clean compile assembly:single`
    - `java -jar target/my-app-1.0-SNAPSHOT-jar-with-dependencies.jar`
### 自行建置maven
1. 參考 maven 配置
2. PowerShell 執行
    - `mvn clean compile assembly:single`
    - `java -jar target/my-app-1.0-SNAPSHOT-jar-with-dependencies.jar`

注意事項
---
JDK 版本配置

`java version "17.0.7" 2023-04-18 LTS`

MAVEN 版本配置

`Apache Maven 3.9.6`