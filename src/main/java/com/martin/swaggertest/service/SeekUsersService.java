package com.martin.swaggertest.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.martin.swaggertest.model.User;
import com.martin.swaggertest.model.Users;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class SeekUsersService {
    private String url = "https://reqres.in/api/users?page=2";
    private String userInfo = "";

    /**
     * Provide User's object by the param userId accepted
     * @param userId
     * @return User object
     */
    public User findSpecificUser(Integer userId) {

        try {
            URI uri = URI.create(url + "&id=" + userId);

            HttpRequest request = HttpRequest.newBuilder(uri).build();

            String body = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString()).body();

            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(body).getAsJsonObject();
            JsonObject data = jsonObject.getAsJsonObject("data");

            Users user = new Gson().fromJson(data, Users.class);

            userInfo = user.getFirstName() + ", " + user.getLastName();

            return new User(userInfo, user.getEmail());
        } catch (Exception e) {
            return null;
        }

    }


}
