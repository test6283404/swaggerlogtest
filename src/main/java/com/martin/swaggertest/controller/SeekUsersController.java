package com.martin.swaggertest.controller;

import com.martin.swaggertest.model.User;
import com.martin.swaggertest.service.SeekUsersService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * The user API
 *
 * @Author Martin
 * @since JDK 17.0.8
 */
@RestController
@Slf4j
@Tag(name = "user", description = "the user API")
public class SeekUsersController {

    @Autowired
    private SeekUsersService seekUsersService;


    /**
     * Accept the param in order to get the specific info of user
     * @param userId
     * @return User object
     */
    @Operation(summary = "Find the specific user", description = "Find the specific user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "400", description = "Invalid input")})
    @GetMapping("/find/user")
    public ResponseEntity<User> findUser(@RequestParam("userId") Integer userId) {
        log.info("您正在使用 findUser API");

        User specificUser = seekUsersService.findSpecificUser(userId);

        if (specificUser != null) {
            log.info("User 存在!");

            return ResponseEntity.ok(specificUser);
        } else {
            log.error("User 並不存在!");

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }
}
